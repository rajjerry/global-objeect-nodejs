console.log('After 5 sec you can see the filename');
console.time('Start timer');
var timeOut = setTimeout(() => {
    console.log('FileName : ', __filename);
    console.log('DirectoryName : ', __dirname);
}, 5000);

// clearTimeout(timeOut); To clear the timeout of timer

var timeOutInt = setInterval(() => {
    console.log('This function will call 5 sec of interval');
    
}, 5000);
clearInterval(timeOutInt);
console.timeEnd('Start timer');

